<?php    
    include('server.php');

    if (isset($_SESSION['username'])){
    }else{
        header('location: Login.php');
    }

    if (isset($_GET['logout'])) {
        session_destroy();
        unset($_SESSION['username']);
        header("location: Login.php");
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Contul meu</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">		
    <link rel="stylesheet" type="text/css" href="style.css">

    <style>
        body{
            background-image: url("IMG/WPP2.jpg");
            background-size: cover;
	        background-repeat: no-repeat;
            background-attachment: fixed;            
        }

        .jumbotron{
            background-color:rgb(77, 221, 166);       
            font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;     
            height: 300px;
            text-align: left; 
            line-height: 1;           
        }
    
    </style>

</head>

<body>

    <div class="jumbotron">
        <p style="margin-left: 5%; font-size: 40px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70);">Auto-Trans: Companie de transport persoane</p>
        <p style="margin-left: 5%; font-size: 35px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70); ">Din Brașov în toată țara</p>        
        <div class="logo">
            <a href="Auto-Trans.php"><img src="IMG/Logo.png"></a>
        </div>
    </div>
        
    <center>
           
        <div class="navbar"  style="height: 67.5px; top: -17.5px;">                 
                           
            <a href="Auto-Trans.php">Auto-Trans</a>
            <a href="Curse.php">Rute interne</a>
            <a href="Despre-noi.php">Despre noi</a>            
            <a href="Contact.php">Contact</a>     
                
        </div>
           
    </center>     
    
    <div style="margin: auto; width: 400px; height: auto; border: 1px solid black; border-radius: 10px; text-align: center;">
        <?php if (isset($_SESSION['username'])): ?>
            <p style="font-size: 30px;">Sunteți logat ca: <strong><?php echo $_SESSION['username']; ?></strong>!</p>
        <?php endif ?>        
        
        <?php if (isset($_SESSION['username'])): ?>       
            <a href="index.php?logout='1'" style="font-size: 30px; color: red; font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;">Logout</a>
        <?php endif ?>
    </div> 
    

</body>
</html>