<!DOCTYPE html>
<html>
<head>
	<title>Auto-Trans</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">  
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>        
        body{
            font-family: "Raleway", Arial, Helvetica, sans-serif;
        }
        .jumbotron{
            background-color: rgb(77, 221, 166);       
            font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;     
            height: 300px;
            text-align: left; 
            line-height: 1;           
        }
    
    </style>

</head>

<body>
    
    <div class="jumbotron">
        <p style="margin-left: 5%; font-size: 40px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70);">Auto-Trans: Companie de transport persoane
            <a href="Account.php" style="text-decoration: none; margin-left: 400px; font-family: 'Courier New', Courier, monospace;">Contul meu</a>
        </p>
        <p style="margin-left: 5%; font-size: 35px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70); ">Din Brașov în toată țara</p>        
        <div class="logo">
                <a href="Auto-Trans.php"><img src="IMG/Logo.png"></a>
        </div>
    </div>

    <center>
   
        <div class="navbar" style="height: 67.5px; top: -17.5px;">             
                   
            <a href="Auto-Trans.php">Auto-Trans</a>
            <a href="Curse.php">Rute interne</a>
            <a href="Despre-noi.php">Despre noi</a>            
            <a href="Contact.php">Contact</a>     
        
        </div>
   
    </center>  
       
    <br>
    
    <div class="container">
        
        <div class="box">            
            <a href="">Închiriere vehicule</a>
            <br>
            <br>
            <p>Posibilitatea închirierii microbuzelor pentru grupuri cu număr fix de persoane.</p>
            <br>
            <button type="button" class="btn-detalii">Detalii</button>            
        </div>
        
        <div class="box1">
            <a href="Curse.php">Curse și orare</a>
            <div style="margin-top:40px; text-align: center;">                                     
                <button type="button" class="btn-curse" onclick="parent.location='Curse.php'">Click pentru a vedea toate cursele</button>
            </div>
        </div>        
       
        <div class="box2">            
            <a href="Rezervari.php">Rezervări online</a>
            <div style="margin-top:40px; text-align: center;">
                <button type="button" class="btn-rez" onclick="parent.location='Rezervari.php'">Rezervă acum</button>
            </div>
        </div>

    </div>    

    <br>
    <br>
    <br>
    <br>

    <div class="w3-container w3-blue" style="width: 70%; margin: auto; border: 1px solid white;">
      <h2><i class="fa fa-map-signs w3-margin-right"></i>Destinații</h2>
    </div>

    <div class="w3-row" style="width: 70%; margin: auto;">
        <div class="w3-col s4">
        <a href="Buc.html"><img src="IMG/bucuresti.jpg" class="image"></a>
        <div class="overlay">București</div>  
        </div>

        <div class="w3-col s4">
        <a href=""><img src="IMG/timisoara.jpg" class="image"></a> 
        <div class="overlay">Timișoara</div>          
        </div>

        <div class="w3-col s4">
        <a href=""><img src="IMG/iasi.jpg" class="image"></a> 
        <div class="overlay">Iași</div>
        </div>

        <div class="w3-col s4" style="margin-top: -70px;">
        <a href=""><img src="IMG/cluj.jpg" class="image"></a> 
        <div class="overlay">Cluj-Napoca</div>
        </div>

        <div class="w3-col s4" style="margin-top: -70px;">
        <a href=""><img src="IMG/oradea.jpg" class="image"></a> 
        <div class="overlay">Oradea</div>
        </div>

        <div class="w3-col s4" style="margin-top: -70px;">
        <a href=""><img src="IMG/craiova.jpg" class="image"></a> 
        <div class="overlay">Craiova</div>
        </div>
    </div>        
   

    <div class="footer">
    <div class="w3-row w3-large w3-center" style="margin: 20px 0;">        
        <i class="fa fa-map-marker w3-text-red" style="width:30px; margin-top: 30px;"></i> Brașov, România
        <i class="fa fa-phone w3-text-red" style="width:30px"></i> Telefon: 0774 664 639
        <i class="fa fa-envelope w3-text-red" style="width:30px"> </i> Email: Auto-Trans-Brasov@yahoo.com
    </div>
    <div>  

</body>

</html>