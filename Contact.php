<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>
        
        .jumbotron{
            background-color:rgb(77, 221, 166);       
            font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;     
            height: 300px;
            text-align: left; 
            line-height: 1;                        
        }
    
    </style>

</head>

<body>

    <div class="jumbotron">
        <p style="margin-left: 5%; font-size: 40px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70);">Auto-Trans: Companie de transport persoane
            <a href="Account.php" style="text-decoration: none; margin-left: 400px; font-family: 'Courier New', Courier, monospace;">Contul meu</a>
        </p>
        <p style="margin-left: 5%; font-size: 35px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70); ">Din Brașov în toată țara</p>
        <div class="logo">
                <a href="Auto-Trans.php"><img src="IMG/Logo.png"></a>
        </div>
    </div>

    <center>
   
        <div class="navbar" style="height: 67.5px; top: -17.5px;">                 
              
            <a href="Auto-Trans.php">Auto-Trans</a>
            <a href="Curse.php">Rute interne</a>
            <a href="Despre-noi.php">Despre noi</a>            
            <a href="Contact.php">Contact</a>     
   
        </div>

    </center>

    <div class="skewbox">
        
        <p style="font-size: 20px;"><i class="fa fa-phone"></i>Telefon: 0774 664 639</p>
        <p style="margin-top: -37px; margin-left: 203px;">0368 445 214</p>
                        
        <p style="font-size: 20px;"><i class="fa fa-map-marker"></i>Adresa: Str. Nicolae Titulescu nr.37, Brașov, România</p>
                
        
        <p style="font-size: 20px;"><i class="fa fa-envelope"></i>E-Mail: Auto-Trans-Brasov@yahoo.com</p>
        <br>
        <br>
        
        <p style="font-size: 20px; color: white; margin-top: -30px;"><i class="fa fa-credit-card"></i>Banca: BCR, 15 Noiembrie nr.92</p>
        <p style="font-size: 20px; margin-top: -37px; margin-left: 200px; color: white;">Cont RON: RO78 RNCB 0087 1674 1908 8892</p>
    </div>
    
    <div class="footer" style="margin-top: 150px;">
    <div class="w3-row w3-large w3-center" style="margin: 20px 0;">        
        <i class="fa fa-map-marker w3-text-red" style="width:30px; margin-top: 30px;"></i> Brașov, România
        <i class="fa fa-phone w3-text-red" style="width:30px"></i> Telefon: 0774 664 639
        <i class="fa fa-envelope w3-text-red" style="width:30px"> </i> Email: Auto-Trans-Brasov@yahoo.com
    </div>
    <div>

</body>
</html>