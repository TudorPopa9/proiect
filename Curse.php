<!DOCTYPE html>
<html>
<head>
	<title>Curse</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">		
    <link rel="stylesheet" type="text/css" href="style.css">
		
	<style>
        
        .jumbotron{
            background-color:rgb(77, 221, 166);       
            font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;     
            height: 300px;
            text-align: left; 
            line-height: 1;                      
        }
    
    </style>

</head>

<body>
    
    <div class="jumbotron">
        <p style="margin-left: 5%; font-size: 40px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70);">Auto-Trans: Companie de transport persoane
            <a href="Account.php" style="text-decoration: none; margin-left: 400px; font-family: 'Courier New', Courier, monospace;">Contul meu</a>
        </p>
        <p style="margin-left: 5%; font-size: 35px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70); ">Din Brașov în toată țara</p>
        <div class="logo">
                <a href="Auto-Trans.php"><img src="IMG/Logo.png"></a>
        </div>
    </div>

    <center>
   
        <div class="navbar" style="height: 67.5px; top: -17.5px;">                 
              
            <a href="Auto-Trans.php">Auto-Trans</a>
            <a href="Curse.php">Rute interne</a>
            <a href="Despre-noi.php">Despre noi</a>            
            <a href="Contact.php">Contact</a>     
   
        </div>

    </center>

    <div class="container1">
        <div class="acordion">
            <section id="item1">
                <a href="#item1" style="text-decoration: none; border-top-left-radius: 10px; border-top-right-radius: 10px;">Brașov-București</a>
                <p>
                    Brașov – Predeal – Azuga – Bușteni – Sinaia – Breaza – Ploiești – București 
                </p>
                <p>Distanța: 170km</p>
                <p>Pret: 25 RON</p>
            </section>

            <section id="item2">
                <a href="#item2" style="text-decoration: none;">Brașov-Iași</a>
                <p>
                    Brașov – Târgu Secuiesc – Onești – Bacău – Roman – Iași
                </p>
                <p>Distanța: 303km</p>
                <p>Pret: 50 RON</p>
            </section>

            <section id="item3">
                <a href="#item3" style="text-decoration: none;">Brașov-Cluj Napoca</a>
                <p>
                    Brașov – Rupea – Sighisoara – Târgu Mureș – Turda – Cluj 
                </p>
                <p>Distanța: 271km</p>
                <p>Pret: 35 RON</p>
            </section>

            <section id="item4">
                <a href="#item4" style="text-decoration: none;">Brașov-Oradea</a>
                <p>
                    Brașov – Rupea – Sighisoara – Târgu Mureș – Turda – Cluj – Huedin – Oradea
                </p>
                <p>Distanța: 416km</p>
                <p>Pret: 60 RON</p>
            </section>

            <section id="item5">
                <a href="#item5" style="text-decoration: none;">Brașov-Craiova</a>
                <p>
                    Brașov – Câmpulung – Mioveni – Pitești – Slatina – Craiova
                </p>
                <p>Distanța: 257km</p>
                <p>Pret: 35 RON</p>
            </section>

            <section id="item6">
                <a href="#item6"  style="text-decoration: none; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; border: none;">Brașov-Timișoara</a>
                <p>
                    Brașov – Fagaraș – Sibiu – Sebeș – Deva – Timișoara
                </p>
                <p>Distanța: 414km</p>
                <p>Pret: 55 RON</p>
            </section>    
        </div> 
    </div>

    <div style="height: 50px;">

    <div class="footer">
    <div class="w3-row w3-large w3-center" style="margin: 20px 0;">        
        <i class="fa fa-map-marker w3-text-red" style="width:30px; margin-top: 30px;"></i> Brașov, România
        <i class="fa fa-phone w3-text-red" style="width:30px"></i> Telefon: 0774 664 639
        <i class="fa fa-envelope w3-text-red" style="width:30px"> </i> Email: Auto-Trans-Brasov@yahoo.com
    </div>
    <div>

</body>

</html>