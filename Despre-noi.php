<!DOCTYPE html>
<html>
<head>
	<title>Despre noi</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
		
	<style>        
        .jumbotron{
            background-color:rgb(77, 221, 166);       
            font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;     
            height: 300px;
            text-align: left; 
            line-height: 1;                               
        }
    </style>

</head>

<body>
    
    <div class="jumbotron">
        <p style="margin-left: 5%; font-size: 40px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70);">Auto-Trans: Companie de transport persoane
            <a href="Account.php" style="text-decoration: none; margin-left: 400px; font-family: 'Courier New', Courier, monospace;">Contul meu</a>
        </p>
        <p style="margin-left: 5%; font-size: 35px; text-shadow: 3px 3px 8px; color:rgb(70, 70, 70); ">Din Brașov în toată țara</p>
        <div class="logo">
                <a href="Auto-Trans.php"><img src="IMG/Logo.png"></a>
        </div>
    </div>      
    
    <center>
   
        <div class="navbar" style="height: 67.5px; top: -17.5px;">                 
                   
            <a href="Auto-Trans.php">Auto-Trans</a>
            <a href="Curse.php">Rute interne</a>
            <a href="Despre-noi.php">Despre noi</a>            
            <a href="Contact.php">Contact</a>     
        
        </div>
   
    </center>

    <div class="background">        
        <div class="title">
            <p>Cine suntem noi?</p>
            <p1>AUTO-TRANS Brașov este una din cele mai mari companii
                de transport persoane din România, acoperind, în prezent,
                cele mai importante trasee interjudețene și urbane pe rute
                cu plecare din Brașov. Secretul succesului stă în strategia
                de dezvoltare a companiei, în organizarea temeinică, precum
                și în munca depusă de echipa CDI pentru a asigura siguranță
                și ritmicitatea curselor, respectarea orelor din programul
                de transport, fidelizarea călătoriilor.              
            </p1>
        </div>        
    </div>    

    <div class="container2">
        <br>        

        <h1 style="font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif; font-size: 50px; color: white;">Principiile noastre</h1>        
        
        <br>

        <div class="box4">
            <h2 style="text-transform: uppercase; font-weight: 800;">Siguranță</h2>
            <br>
            <br>
            <p>În domeniul transportului de persoane, siguranța călătorului este esențială pentru noi, iar aceasta certitudine este o valoare care o respectăm pe toate palierele de interacțiune si in toate relațiile și acțiunile noastre indiferent dacă relaționăm cu mediul de afaceri, cu societatea sau cu angajații nostri.</p>
        </div>
        
        <div class="box4">
            <h2 style="text-transform: uppercase; font-weight: 800;">Calitate și confort</h2>
            <br>
            <br>
            <p>Pentru a asigura gradul ridicat de confort clienților nostri, punem la dispozitie doar microbuze și autocare noi, dotate corespunzător pentru o calatorie cat mai placută.<br><br><br></p>
        </div>
        
        <div class="box4" style="color: black">
            <h2 style="text-transform: uppercase; font-weight: 800;">Respect</h2>
            <br>
            <br>
            <p>În toate acțiunile noastre, precum și în toată activitatea noastră, ne ghidăm după RESPECT: în relația cu clienții noștri, în relația cu partenerii noștri de afaceri, în relația cu angajații noștri și în relația cu mediul înconjurător. Respectul reciproc stă la baza tuturor relațiilor noastre.</p>        
        </div>  
    </div>

    <div class="footer" style="margin-top: 400px;">
    <div class="w3-row w3-large w3-center" style="margin: 20px 0;">        
        <i class="fa fa-map-marker w3-text-red" style="width:30px; margin-top: 30px;"></i> Brașov, România
        <i class="fa fa-phone w3-text-red" style="width:30px"></i> Telefon: 0774 664 639
        <i class="fa fa-envelope w3-text-red" style="width:30px"> </i> Email: Auto-Trans-Brasov@yahoo.com
    </div>
    <div>

</body>

</html>   
