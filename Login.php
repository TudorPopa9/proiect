<?php include('server.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>
        body{
            background-color: rgb(240, 240, 240);
        }
    </style>

</head>

<body>
    <div class="logo1" style="background-color: white;">
        <a href="Auto-Trans.php"><img src="IMG/Logo.png" style="width: 290px; height: 290px; margin-top: -50px;"></a>
    </div>

    <div class="header">
        Log in
    </div>

    <form method="POST" action="Login.php">
   
        <?php include('errors.php'); ?>

        <div class="input-group">
            <label>Username</label>
            <input type="text" name="username" placeholder="Username">
        </div>

        <div class="input-group">
            <label>Password</label>
            <input type="password" name="password" placeholder="Parola">
        </div>

        <div class="input-group">
            <button type=submit name="login_user" class="btn-login">Log in</button>            
        </div>

        <div class="input-group">  
             <a href="Register.php" style="margin-left: 5px;">Nu aveti cont? Click aici!</a>
        </div>
    </form>

</body>
</html>    