<?php include('server.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">		 
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>
        body{
            background-color: rgb(240, 240, 240);
        }
    </style>

</head>

<body>
<div class="logo1" style="background-color: white;">
        <a href="Auto-Trans.php"><img src="IMG/Logo.png" style="width: 290px; height: 290px; margin-top: -50px;"></a>
    </div>

    <div class="header">
        Register
    </div>

    <form method="POST" action="Register.php">

        <?php include('errors.php'); ?>
        
        <div class="input-group">
            <label>Nume si prenume</label>
            <input type="text" name="nume" placeholder="Nume și prenume">
        </div>

        <div class="input-group">
            <label>Username</label>
            <input type="text" name="username" placeholder="Username">
        </div>

        <div class="input-group">
            <label>Email</label>
            <input type="text" name="email" placeholder="Email">
        </div>

        <div class="input-group">
            <label>Telefon</label>
            <input type="text" name="telefon" placeholder="Nr. telefon">
        </div>

        <div class="input-group">
            <label>Parola</label>
            <input type="password" name="password1" placeholder="Parola">
        </div>

        <div class="input-group">
            <label>Confirmare parola</label>
            <input type="password" name="password2" placeholder="Confirmare parola">
        </div>

        <div class="input-group">
            <button type=submit name="reg_user" class="btn-register">Register</button>            
        </div>
        
    </form>

    <div style="height: 100px;"></div>

</body>
</html>    