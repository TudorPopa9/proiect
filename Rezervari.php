<?php    
    include('server.php');

    if (isset($_SESSION['username'])){
    }else{
        header('location: Login.php');
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Rezervari</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">		
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">	 
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>
        body{
            background-image: url("IMG/WPP3.jpg");
            background-size: cover;
	        background-repeat: no-repeat;
            background-attachment: fixed;  
            font-family: "Raleway", Arial, Helvetica, sans-serif;          
        }
    </style>

</head>

<body>     
    <div class="logo1" style="background-color: white;">
        <a href="Auto-Trans.php"><img src="IMG/Logo.png" style="width: 290px; height: 290px; margin-top: -50px;"></a>
    </div>

    <div class="w3-display-middle w3-padding w3-col l6 m8" style="text-align: center; margin-top: 100px;">
    
    <div class="w3-container w3-red">
      <h2><i class="fa fa-bus w3-margin-right"></i>Detalii rezervare</h2>
    </div>
    
    <div class="w3-container w3-white w3-padding-16">
      
    <form1 method="" action="Rezervari.php">
        
      <div class="w3-row-padding">
          <div class="w3-margin-bottom">
            <label><i class="fa fa-calendar-o"></i> Alege data</label>
            <input class="w3-input w3-border" type="date" min="" max="2018-12-31" name="data" required>
          </div>         
        </div>

        <div class="w3-row-padding">
          <div class="w3-margin-bottom">
            <label><i class="fa fa-arrows-alt"></i> Alege cursa</label>            
            <select class="w3-select" name="destinatii" required>
                <option>Brașov-București</option>
                <option>Brașov-Iași</option>
                <option>Brașov-Timișoara</option>
                <option>București-Brașov</option>
                <option>Iași-Brașov</option>
                <option>Timișoara-Brașov</option>            
            </select>
          </div>          
        </div>

        <div class="w3-row-padding">
          <div class="w3-margin-bottom">
            <label><i class="fa fa-male"></i> Număr persoane</label>
            <input class="w3-input w3-border" type="number" value="1" name="persoane" min="1" max="4" required>
          </div>          
        </div>
        
        <button class="w3-button w3-dark-grey" type="submit"><i class="	fa fa-check-square-o w3-margin-right"></i> Trimite rezervarea</button>
      </form>
    </div>   
    </div>
</body>
</html>