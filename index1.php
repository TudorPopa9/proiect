<?php 
  session_start();

  if (!isset($_SESSION['username'])) {    
        
    header('location: Login.php');
}
  
  if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: Login.php");
  }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Index</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="IMG/logo.png" rel="shortcut icon" type="image/png">	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	 
    <link rel="stylesheet" type="text/css" href="style.css">
      
    
	<style>
        body{
            background-color: rgb(240, 240, 240);
        }
    </style>

</head>

<body>
    <div class="logo1" style="background-color: white;">
        <a href="Auto-Trans.php"><img src="IMG/Logo.png" style="width: 290px; height: 290px; margin-top: -50px;"></a>
    </div>

    <div class="header">
        WELCOME BACK
    </div>

    <form>
        <?php if (isset($_SESSION['success'])): ?>
            <div class="success">
               <h3>
                  <?php 
                    echo $_SESSION['success'];
                    unset($_SESSION['success']);
                  ?>      
               </h3> 
            </div>
        <?php endif ?>

        <?php if (isset($_SESSION['username'])): ?>            
            <p>Bine ai revenit <strong><?php echo $_SESSION['username']; ?></strong>! <a href="Auto-Trans.php">Înapoi la prima pagină</a></p>
            <a href="index.php?logout='1'">Logout</a>
            <a href="Rezervari.php" style="display: inline-block; margin-left: 210px;">Rezervă-ți locul acum!</a>
        <?php endif ?>
    </form>
    

</body>
</html>    