<?php
    session_start();

    $nume = "";    
    $username = "";
    $email = "";
    $telefon = "";
    $errors = array();
    $_SESSION['success'] = "";

    $db = mysqli_connect('localhost', 'root', '', 'trans_db');

    //REGISTER
    if (isset($_POST['reg_user'])) {
        $nume = mysqli_real_escape_string($db, $_POST['nume']);
        $username = mysqli_real_escape_string($db, $_POST['username']);
        $email = mysqli_real_escape_string($db, $_POST['email']);
        $telefon = mysqli_real_escape_string($db, $_POST['telefon']);
        $password1 = mysqli_real_escape_string($db, $_POST['password1']);
        $password2 = mysqli_real_escape_string($db, $_POST['password2']);

        if (empty($nume)) {
            array_push($errors, "Numele este necesar");
        }
        if (empty($username)) {
            array_push($errors, "Usernameul este necesar");
        }
        if (empty($email)) {
            array_push($errors, "Emailul este necesar");
        }
        if (empty($telefon)) {
            array_push($errors, "Telefonul este necesar");
        }
        if (empty($password1)) {
            array_push($errors, "Parola este necesară");
        }

        if ($password1 != $password2) {
            array_push($errors, "Parolele nu coincid");
        }

        $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
        $result = mysqli_query($db, $user_check_query);
        $user = mysqli_fetch_assoc($result);
  
        if ($user) { 
        if ($user['username'] === $username) {
        array_push($errors, "Username indisponibil");
        }

        if ($user['email'] === $email) {
        array_push($errors, "Email indisponibil");
        }

        if ($user['telefon'] === $telefon) {
            array_push($errors, "Numar de telefon indisponibil");
        }
    }

        if(count($errors) == 0) {
            $password = md5($password1);
            $sql = "INSERT INTO users (nume, username, email, telefon, parola)
                    VALUES ('$nume', '$username', '$email', '$telefon', '$password')";
            mysqli_query($db, $sql); 
            $_SESSION['username'] = $username;  
            $_SESSION['success'] = "Autentificare reușită";  
            header('location: index.php');     
        }
    }
    //LOGIN
    if (isset($_POST['login_user'])) {
        $username = mysqli_real_escape_string($db, $_POST['username']);       
        $password = mysqli_real_escape_string($db, $_POST['password']);

        if (empty($username)) {
                array_push($errors, "Usernameul este necesar");
        }
        if (empty($password)) {
                array_push($errors, "Parola este necesară");
        }

        if (count($errors) == 0) {
            $password = md5($password);
            $sql = "SELECT * FROM users WHERE username='$username' AND parola='$password'";
            $results = mysqli_query($db, $sql);

            if (mysqli_num_rows($results) == 1) {
                $_SESSION['username'] = $username;  
                $_SESSION['success'] = "Logare reușită";  
                header('location: index1.php');
            }
            else{
                array_push($errors, "username sau parolă greșită");                    
            }
        }
    }

    
?>
